// eslint-disable-next-line
const { addBabelPlugin, override, useBabelRc } = require('customize-cra');

module.exports = override(
  useBabelRc(),
  addBabelPlugin([
    'babel-plugin-root-import',
    {
      rootPathSuffix: 'src',
    },
  ]),
);
