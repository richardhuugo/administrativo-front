import { gql } from '@apollo/client';

export const LISTAR_EMPRESAS = gql`
  query getEmpresas($first: Int, $page: Int) {
    empresas(first: $first, page: $page) {
      edges {
        node {
          id
          nm_fantasia
          documento {
            nr_documento
          }
        }
      }
      pageInfo {
        startCursor
        endCursor
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;
