import { gql } from '@apollo/client';

export const CADASTRAR_EMPRESA = gql`
  mutation cadastrarEmpresa(
    $nm_fantasia: String!
    $razao_social: String!
    $email: String
    $telefone: ID!
    $documento: String
    $tipo_documento_id: String!
  ) {
    createEmpresa(
      nm_fantasia: $nm_fantasia
      razao_social: $razao_social
      email: $email
      telefone: $telefone
      documento: $documento
      tipo_documento_id: $tipo_documento_id
    ) {
      id
    }
  }
`;
