import { gql } from '@apollo/client';

export const LISTAR_TIPOS_DOCUMENTOS = gql`
  query {
    select_empresa {
      empresa {
        id
        nm_fantasia
        logo
        documento {
          nr_documento
        }
      }
    }
  }
`;
