import React from 'react';
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom';
import './scss/style.scss';

const Login = React.lazy(() => import('./views/pages/login/Login'));
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

export default function App() {
  return (
    <HashRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route
            exact
            path="/login"
            name="Login Page"
            render={props => <Login {...props} />}
          />
          <Route
            path="/"
            name="Home"
            render={props => <TheLayout {...props} />}
          />
          <Redirect to="/" />
        </Switch>
      </React.Suspense>
    </HashRouter>
  );
}
