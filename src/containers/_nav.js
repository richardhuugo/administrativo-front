const _nav = [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Configurações'],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Cadastro',
    route: '/base',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Empresa',
        to: '/empresa',
      },
    ],
  },
];

export default _nav;
