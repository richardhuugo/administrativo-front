import React from 'react';

const Empresa = React.lazy(() => import('../views/pages/empresas'));
const NovaEmpresa = React.lazy(() => import('../views/pages/empresas/new'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/empresa', name: 'Empresas', component: Empresa, exact: true },
  {
    path: '/empresa/novo-cadastro',
    name: 'Empresa - Novo cadastro',
    component: NovaEmpresa,
    exact: true,
  },
];

export default routes;
