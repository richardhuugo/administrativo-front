import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  AUTH_PROVIDER_CHANGE,
  SOLICITAR_SENHA_REQUEST,
  SOLICITAR_SENHA_SUCCESS,
  SOLICITAR_SENHA_FAILED,
  ALTERAR_SENHA_SUCCESS,
} from '~/config/constantes';

/**
 * LOGIN
 */
export const ProviderAuthChange = payload => ({
  type: AUTH_PROVIDER_CHANGE,
  payload,
});
export const LoginRequest = payload => ({
  type: LOGIN_REQUEST,
  payload,
});

export const LoginSuccess = payload => ({
  type: LOGIN_SUCCESS,
  payload,
});
export const LoginFailed = payload => ({
  type: LOGIN_FAILED,
  payload,
});

export const SolicitarSenhaRequest = payload => ({
  type: SOLICITAR_SENHA_REQUEST,
  payload,
});

export const SolicitarSenhaSuccess = payload => ({
  type: SOLICITAR_SENHA_SUCCESS,
  payload,
});
export const SolicitarSenhaFailed = payload => ({
  type: SOLICITAR_SENHA_FAILED,
  payload,
});

export const AlterarSenhaUpdateToken = payload => ({
  type: ALTERAR_SENHA_SUCCESS,
  payload,
});

export const InvalidateToken = () => {};
