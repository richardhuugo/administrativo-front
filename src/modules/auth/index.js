import { PURGE } from 'redux-persist';
import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  AUTH_PROVIDER_CHANGE,
  SOLICITAR_SENHA_REQUEST,
  SOLICITAR_SENHA_SUCCESS,
  SOLICITAR_SENHA_FAILED,
} from '~/config/constantes';

const INITIAL_STATE = {
  access_token: null,
  logged: false,
  loggedAt: null,
  loading: false,
  user_id: null,
  empresa_ativa: null,
  contrato_ativo: null,
};

function Auth(state = INITIAL_STATE, action) {
  switch (action.type) {
    case PURGE:
      return INITIAL_STATE;

    case AUTH_PROVIDER_CHANGE: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case LOGIN_REQUEST: {
      return {
        ...state,
        logged: false,
        loading: true,
      };
    }
    case LOGIN_SUCCESS: {
      const { access_token, user_id } = action.payload.data;
      return {
        ...state,
        loading: false,
        logged: true,
        user_id,
        loggedAt: new Date().toJSON(),
        access_token: `Bearer ${access_token}`,
      };
    }
    case LOGIN_FAILED: {
      return {
        ...state,
        loading: false,
        logged: false,
        access_token: null,
      };
    }
    case SOLICITAR_SENHA_REQUEST: {
      return {
        ...state,
        loading: true,
      };
    }
    case SOLICITAR_SENHA_SUCCESS: {
      return {
        ...state,
        send_recuperar: true,
        loading: false,
      };
    }
    case SOLICITAR_SENHA_FAILED: {
      return {
        ...state,
        loading: false,
      };
    }
    default:
      return state;
  }
}

export default Auth;
