import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';
import AuthReducer from './auth/index';
import UtilReducer from './utils';

const rootReducer = combineReducers({
  toastr: toastrReducer,
  auth: AuthReducer,
  util: UtilReducer,
});

export default rootReducer;
