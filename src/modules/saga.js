import { all } from 'redux-saga/effects';
import AuthSaga from './auth/sagas';
export default function* rootSaga() {
  return yield all([AuthSaga]);
}
