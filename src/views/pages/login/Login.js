import React from 'react';
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from '@coreui/react';
import { useForm } from 'react-hook-form';
import CIcon from '@coreui/icons-react';
import CustomInputMask from '../../../components/mask_input';

const Login = () => {
  const { register, handleSubmit } = useForm();
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <CForm
                    onSubmit={handleSubmit(async data => {
                      console.log(data);
                    })}
                  >
                    <h1>Login</h1>
                    <p className="text-muted">
                      Informe os dados abaixo para acessar o ambiente
                    </p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CustomInputMask
                        name="cpf"
                        mask="999.999.999-99"
                        Component={props => <CInput {...props} />}
                        register={register({ required: true })}
                        maskChar="_"
                        required
                        placeholder="xxx.xxx.xxx-xx"
                      />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        innerRef={register({ required: true })}
                        name="password"
                        placeholder="Password"
                        autoComplete="current-password"
                      />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="link" className="px-0">
                          Esqueci minha senha
                        </CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton type="submit" color="primary" className="px-4">
                          Login
                        </CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
