/* eslint-disable react/jsx-boolean-value */
/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { useForm } from 'react-hook-form';
import {
  CButton,
  CCard,
  CCardBody,
  CInvalidFeedback,
  CCardHeader,
  CCol,
  CCollapse,
  CFade,
  CForm,
  CFormGroup,
  CInput,
  CLabel,
  CRow,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';
import CustomInputMask from '../../../../components/mask_input';

const NovaEmpresa = () => {
  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm();
  const [collapsed, setCollapsed] = React.useState(true);
  const [showElements, setShowElements] = React.useState(true);

  const onSubmit = data => {
    console.log(data);
  };

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CFade timeout={300} in={showElements} unmountOnExit={true}>
            <CCard>
              <CCardHeader>
                Formulário de cadastro
                <div className="card-header-actions">
                  <CButton
                    color="link"
                    className="card-header-action btn-setting"
                  >
                    <CIcon name="cil-settings" />
                  </CButton>
                  <CButton
                    color="link"
                    className="card-header-action btn-minimize"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <CIcon
                      name={collapsed ? 'cil-arrow-top' : 'cil-arrow-bottom'}
                    />
                  </CButton>
                  <CButton
                    color="link"
                    className="card-header-action btn-close"
                    onClick={() => setShowElements(false)}
                  >
                    <CIcon name="cil-x" />
                  </CButton>
                </div>
              </CCardHeader>
              <CCollapse show={collapsed} timeout={1000}>
                <CCardBody>
                  <CForm
                    onSubmit={handleSubmit(onSubmit)}
                    encType="multipart/form-data"
                    className="form-horizontal"
                  >
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="password-input">CNPJ</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CustomInputMask
                          name="cnpj"
                          mask="999.999.999-99"
                          Component={props => <CInput {...props} />}
                          register={register({ required: true })}
                          maskChar="_"
                          required
                          placeholder="Informe o CNPJ da empresa"
                        />
                        {errors?.cnpj && (
                          <CInvalidFeedback>
                            Você deve informar o CNPJ da empresa
                          </CInvalidFeedback>
                        )}
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="text-input">Razão Social</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput
                          type="text"
                          invalid={errors?.razao_social}
                          innerRef={register({ required: true })}
                          name="razao_social"
                          placeholder="Informe a razão social"
                        />
                        {errors?.razao_social && (
                          <CInvalidFeedback>
                            Você deve informar a razão social
                          </CInvalidFeedback>
                        )}
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel>Nome Fantasia</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput
                          type="text"
                          invalid={errors?.nome_fantasia}
                          innerRef={register({ required: true })}
                          name="nome_fantasia"
                          placeholder="Informe o nome fantasia"
                        />
                        {errors?.nome_fantasia && (
                          <CInvalidFeedback>
                            Você deve informar o nome fantasia
                          </CInvalidFeedback>
                        )}
                      </CCol>
                    </CFormGroup>

                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="date-input">E-mail</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CInput
                          type="email"
                          invalid={errors?.email}
                          innerRef={register({ required: true })}
                          placeholder="Informe o e-mail"
                          name="email"
                        />
                        {errors?.email && (
                          <CInvalidFeedback>
                            Você deve informar o e-mail
                          </CInvalidFeedback>
                        )}
                      </CCol>
                    </CFormGroup>
                    <CFormGroup row>
                      <CCol md="3">
                        <CLabel htmlFor="date-input">Telefone</CLabel>
                      </CCol>
                      <CCol xs="12" md="9">
                        <CustomInputMask
                          name="telefone"
                          mask="(99) 9999-9999"
                          invalid={errors?.telefone}
                          Component={props => <CInput {...props} />}
                          register={register({ required: true })}
                          maskChar="_"
                          placeholder="Informe o telefone"
                        />
                        {errors?.telefone && (
                          <CInvalidFeedback>
                            Você deve informar o telefone
                          </CInvalidFeedback>
                        )}
                      </CCol>
                    </CFormGroup>

                    <div
                      className="form-actions"
                      style={{ display: 'flex', justifyContent: 'flex-end' }}
                    >
                      <CButton color="secondary">Cancelar</CButton>
                      <CButton type="submit" color="primary">
                        Cadastrar
                      </CButton>
                    </div>
                  </CForm>
                </CCardBody>
              </CCollapse>
            </CCard>
          </CFade>
        </CCol>
      </CRow>
    </>
  );
};

export default NovaEmpresa;
