/* eslint-disable react-hooks/rules-of-hooks */
import { useQuery } from '@apollo/client';
import { LISTAR_TIPOS_DOCUMENTOS } from '../config/query-gql';

export const capturarTipoDocumentos = () => {
  const elementos = useQuery(LISTAR_TIPOS_DOCUMENTOS, {
    fetchPolicy: 'cache',
  });

  return elementos;
};
