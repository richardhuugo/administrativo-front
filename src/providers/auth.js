import { capturarTipoDocumentos } from './actions';
import { AuthContext } from './contexto';

export default function AuthProvider({ children }) {
  const tipoDocumentos = capturarTipoDocumentos();

  const contextoObject = {
    tipoDocumentos,
  };
  return (
    <AuthContext.Provider value={contextoObject}>
      {children}
    </AuthContext.Provider>
  );
}
