import { createContext, useContext } from 'react';

export const AuthContext = createContext({
  isAuthenticated: false,
  tipo_documentos: [],
});
export default function useAuth() {
  return useContext(AuthContext);
}
