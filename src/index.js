import React from 'react';
import { ApolloProvider } from '@apollo/client';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import 'react-app-polyfill/ie11'; // For IE 11 support
import 'react-app-polyfill/stable';
import 'core-js';
import './polyfill';
import App from './App';
import store from './store';
import { icons } from './assets/icons';
import ClienteApollo from './config/apollo';

React.icons = icons;

ReactDOM.render(
  <Provider store={store}>
    <ApolloProvider client={ClienteApollo}>
      <App />
    </ApolloProvider>
  </Provider>,
  document.getElementById('root'),
);
