import React, { useEffect, useState } from 'react';
import { InputMaskStyle } from './styles';

export default function CustomInputMask({
  name,
  mask,
  maskChar = '',
  defaultValue = '',
  placeholder,
  className,
  onChange = () => {},
  register = () => {},
  reset = false,
  Component = props => {
    return <input {...props} />;
  },
  ...rest
}) {
  const [initValue, setInitValue] = useState(defaultValue);

  useEffect(() => {
    if (reset) {
      setInitValue('');
    }
  }, [reset]);

  return (
    <InputMaskStyle
      mask={mask}
      maskChar={maskChar}
      placeholder={placeholder}
      className={className}
      onChange={e => {
        setInitValue(e.target.value);
        onChange(e);
      }}
      value={initValue}
      // onFocus={onFocus}
      {...rest}
    >
      {props => (
        <Component {...props} ref={register} innerRef={register} name={name} />
      )}
    </InputMaskStyle>
  );
}
