/* eslint-disable react/jsx-boolean-value */
/* eslint-disable react/no-unescaped-entities */
import React from 'react';
import { useForm } from 'react-hook-form';
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CCollapse,
  CFade,
  CForm,
  CRow,
} from '@coreui/react';
import CIcon from '@coreui/icons-react';

const CustomForm = ({
  handleSubmit,
  onSubmit,
  headerTitle = 'Formulário de cadastro',
  children,
}) => {
  const [collapsed, setCollapsed] = React.useState(true);
  const [showElements, setShowElements] = React.useState(true);

  return (
    <>
      <CRow>
        <CCol xs="12">
          <CFade timeout={300} in={showElements} unmountOnExit={true}>
            <CCard>
              <CCardHeader>
                {headerTitle}
                <div className="card-header-actions">
                  <CButton
                    color="link"
                    className="card-header-action btn-setting"
                  >
                    <CIcon name="cil-settings" />
                  </CButton>
                  <CButton
                    color="link"
                    className="card-header-action btn-minimize"
                    onClick={() => setCollapsed(!collapsed)}
                  >
                    <CIcon
                      name={collapsed ? 'cil-arrow-top' : 'cil-arrow-bottom'}
                    />
                  </CButton>
                  <CButton
                    color="link"
                    className="card-header-action btn-close"
                    onClick={() => setShowElements(false)}
                  >
                    <CIcon name="cil-x" />
                  </CButton>
                </div>
              </CCardHeader>
              <CCollapse show={collapsed} timeout={1000}>
                <CCardBody>
                  <CForm
                    onSubmit={handleSubmit(onSubmit)}
                    encType="multipart/form-data"
                    className="form-horizontal"
                  >
                    {[children]}
                    <div
                      className="form-actions"
                      style={{ display: 'flex', justifyContent: 'flex-end' }}
                    >
                      <CButton color="secondary">Cancelar</CButton>
                      <CButton type="submit" color="primary">
                        Cadastrar
                      </CButton>
                    </div>
                  </CForm>
                </CCardBody>
              </CCollapse>
            </CCard>
          </CFade>
        </CCol>
      </CRow>
    </>
  );
};

export default CustomForm;
